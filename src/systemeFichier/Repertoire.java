package systemeFichier;

import java.util.ArrayList;

public class Repertoire extends Composant{
	
	ArrayList<Composant> list;
	ArrayList<Repertoire> path;
	
	public Repertoire(String nom) {
		super(nom);
		list = new ArrayList<>() ;
		path = new ArrayList<>();
	}
	
public void Add(Composant comp){
			if(comp.getClass()==Repertoire.class){
					if(!comp.equals(this) && !path.contains(comp)) {
							list.add(comp);
							((Repertoire)comp).path=this.path;
							((Repertoire) comp).path.add(this);
							System.out.println(comp.getNom()+" HAS BEEN ADDED TO " +getNom()+"\n");
		   }
	    else System.out.println("\n you can't add the same file ");
		  
}
else list.add(comp);  
		}
		
	public void Remove(Composant comp){
				list.remove(comp);
			}
	
	@Override
	public int CalculeTaille() {
				
		int TotalSomme=this.getTaille();
		for ( Composant l : this.list) {
			TotalSomme+=l.CalculeTaille();
			}
		return TotalSomme;	
		}
	
	

}
