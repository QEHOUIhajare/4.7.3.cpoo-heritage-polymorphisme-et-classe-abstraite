package systemeFichier; 

public abstract class Composant {
	
	protected int taille;
	protected String nom;
	public abstract  int CalculeTaille() ;
	
	public Composant(int taille, String nom) {
		
		this.taille = taille;
		this.nom = nom;
	}
	
	public Composant(String nom) {
		
		this.nom = nom;
		this.taille = 0 ;
	}
	
	public Composant() {
		
	}

	public int getTaille() {
		return taille;
	}

	public void setTaille(int taille) {
		this.taille = taille;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	

}
