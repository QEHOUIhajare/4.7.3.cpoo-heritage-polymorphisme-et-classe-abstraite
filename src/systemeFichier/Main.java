package systemeFichier;

public class Main {

	public static void main(String[] args) {
		
		Fichier fic1 = new Fichier(13,"fichier 1");
		Fichier fic2 = new Fichier(15,"fichier 2");
		
		Repertoire r1 =new Repertoire("Rep1");
		Repertoire r2 =new Repertoire("Rep2");
		Repertoire r3 =new Repertoire("Rep3");
		r1.Add(fic1);
		r1.Add(fic2);
		r1.Add(r2);
		System.out.println("la taille de R1 est : "+r1.CalculeTaille());
		System.out.println("la taille de R2 est : "+r2.CalculeTaille());
		r2.Add(fic2);
		r3.Add(fic1);
		r2.Add(r3);
		
		System.out.println("la taille de R1 est : "+r1.CalculeTaille());
		System.out.println("la taille de R2 est : "+r2.CalculeTaille());
		System.out.println("la taille de R3 est : "+r3.CalculeTaille());
		r3.Remove(fic1);
		r2.Remove(r3);
		
		System.out.println("la taille de R1 est : "+r1.CalculeTaille());
		System.out.println("la taille de R2 est : "+r2.CalculeTaille());
		System.out.println("la taille de R3 est : "+r3.CalculeTaille());

	}

}
